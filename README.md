# Tango Workshop

## Setup a virtual environment with PyTango

Note: itango requires python <3.12

    ``` bash
    $ python3.11 -m venv venv
    ...
    $ source venv/bin/activate
    ...
    (venv) $ pip install --require-virtualenv pytango itango
    ...
    ```

## Run DB-less examples from Tango_Controls_for_beginners/

These examples are courtesy of Thomas Juerges at SKAO.  They have nice
instructions at the top about how to run them.

    ``` bash
    (venv) $ cd Tango_Controls_for_beginners/part1/device
    (venv) $ head SimplestTangoDevice.py
    '''
    Run this Python3 script on your local machine like this:
    python3 SimplestTangoDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

    Then connect to the Device from the same machine like this in iTango:
    dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
    '''
    ...
    (venv) $ python3 SimplestTangoDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1
    Ready to accept request
    ```

In another terminal:

    ``` bash
    (venv) $ itango3
    In [1] = dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
    ```

## Startup Tango test environment and connect with itango

Startup the environment with docker compose:

    ``` bash
    (venv) $ docker compose up -d
    ✔ Network tango-net                Created                                                                                                                            0.1s
    ✔ Container tcworkshop-tango-db-1    Started                                                                                                                            0.0s
    ✔ Container tcworkshop-tango-dbds-1  Started                                                                                                                            0.0s
    ✔ Container tcworkshop-tango-test-1  Started
    ```

Connect to TangoTest device with itango:

    ``` bash
    (venv) $ TANGO_HOST=localhost:10000 itango3
    ...
    In [1]: dp = Device("sys/tg_test/1")
    ```
